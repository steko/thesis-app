from tesi.models import Site, Context, Region

from StringIO import StringIO

def biblio():
    result = StringIO()

    for r in Region.objects.all():
        result.write('\section*{%s}\n\n' % r.nam.title())
        result.write('\\begin{description}\n')
        for s in Site.objects.filter(point__within=r.geom):
            cites = []
            for c in s.context_set.all():
                for line in c.biblio.splitlines():
                    if line.startswith('@'):
                        key = line.partition('{')[2].rstrip(',')
                        cites.append(key)
            cites = set(cites)
            if cites:
                result.write('\item[%s] \cite{' % s.name.strip())
                result.write(','.join(cites))
                result.write('}.\n')
        result.write('\\end{description}\n\n\n')

    print result.getvalue()

def missing():
    for s in Site.objects.all():
        cites = []
        for c in s.context_set.all():
            for line in c.biblio.splitlines():
                if line.startswith('@'):
                    key = line.partition('{')[2].rstrip(',')
                    cites.append(key)
        cites = set(cites)
        if not cites:
            print('%s\n' % s.name)

missing()
