from tesi.models import Site, SherdCount

from StringIO import StringIO

result = StringIO()
result.write('"Sito","Totale","Forme","Durata"\n')

for s in Site.objects.all():
    forme = []
    for sc in SherdCount.objects.filter(assemblage__context__site=s):
        forme.append(sc.typename)
    forme = len(set(forme))
    try:
        result.write('"%s",%s,%s,%s\n' % (s.name, s.total, forme, s.final_import_date - s.initial_import_date))
    except TypeError:
        continue

salva = open("statsiti.csv", 'w')
salva.write(result.getvalue())
salva.close()
