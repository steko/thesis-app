from django.conf import settings

from tesi.models import *
from tesi.mpi import main

'''
ARSW in Byzantine Italy
~~~~~~~~~~~~~~~~~~~~~~~

This script can be executed straight from the terminal, without
running ``manage.py``. Just make sure the database server is up!

    DJANGO_SETTINGS_MODULE=settings python steko_bizantini.py

'''

YEARS = (559, 584, 609, 625, 645)

def get_quantitative():
    '''Extract quantitative data from each site in the Byzantine area.'''

    for y in YEARS:
        empire = Empire.objects.filter(periodo__gt=y).unionagg()
        sites = Site.objects.filter(point__within=empire)
        actual_sites = [ s for s in sites if s.final_import_date >= y and s.initial_import_date <= y ]
        #print('%s,%s' % (y, actual_sites))
        print('%s done!' % y)

    types = Type.objects.filter(start_date__isnull=False
        ).filter(end_date__isnull=False)
    data = [
        [
            t.name,
            t.start_date,
            t.end_date,
            sum(sc.value for sc in t.sherdcount_set.filter(assemblage__context__site__in=actual_sites))]
        for t in types]
    table = main(data)
    print(table) # need to save this to a CSV file

def run():
    print('Anno,AreaBizantina,Totale')
    for y in YEARS:
        empire = Empire.objects.filter(periodo__gt=y).unionagg()
        total_sites = [ s for s in Site.objects.all() if s.final_import_date >= int(y) and s.initial_import_date <= int(y) ]
        sites = Site.objects.filter(point__within=empire)
        actual_sites = [ s for s in sites if s.final_import_date >= y and s.initial_import_date <= y ]
        print('%s,%s,%s' % (y, len(actual_sites),len(total_sites)))


if __name__ == "__main__":
    get_quantitative()
