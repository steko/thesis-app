=============================================
 African Red Slip Ware in Late Antique Italy
=============================================

This is a Django application that I wrote for my Master's thesis in
Late Antique Archeology. The aim of my research was (and still is) to
investigate distribution patterns based on a large number of sites
from literature.

What can I do with this software?
=================================

You can do 2 things with this software:

1. run it as it is, and browse the dataset
2. adapt it to your own research

This software is released under the GNU General Public License v3,
giving you the freedom to do these things!

What do I need to run the software?
===================================

There are more detailed instructions for Ubuntu 11.04, but in any case
you need:

- Python
- Django
- a spatial database (SpatiaLite is easier)
- a web server if you're building a public-facing website

What is Django?
===============

Django_ is a “web framework for perfectionists with deadlines” written
in Python. If you can write Python, you can write a Django
application.

.. _Django: http://www.djangoproject.com/

This is not a database! Give me a database file, please
=======================================================

This is not a question (but this was not supposed to be a FAQ,
indeed). Anyway, that's true, this is not a database: there's also a
web application, a GIS, a gazeteer and a catalogue.
