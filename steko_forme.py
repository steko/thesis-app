from tesi.models import Type

import StringIO

result = StringIO.StringIO()
result.write("Nome,Forma,Inizio,Fine,Totale\n")

forme = Type.objects.all()

for f in forme:
    result.write('"%s", "%s", %s, %s, %s\n' % (f.name, f.form, f.start_date, f.end_date, f.total))

salva = open("forme-conteggi.csv", 'w')
salva.write(result.getvalue())
salva.close()
