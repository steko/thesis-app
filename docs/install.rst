

Using SpatiaLite for local testing
----------------------------------

On Ubuntu Linux 11.10, install the following packages:

- python-pysqlite2
- python-django
- python-django-south
- libspatialite2
- spatialite-bin (needed just to initialise the spatial metadata tables)

See also: http://docs.djangoproject.com/en/dev/ref/contrib/gis/install/#creating-a-spatial-database-for-spatialite

Before running ``syncdb``, create the database file with ``spatialite``::

    spatialite yourdbname.sqlite

and then initialise the spatial metadata tables::

    SELECT InitSpatialMetaData();

After that you can exit the program with ``.q`` and go on with ``syncdb``::

    ./manage.py syncdb

This will install all the tables. It will also ask you to create a super user if
you want. Follow the instructions for migrating the app with South::

    ./manage.py migrate tesi

Only if you already have a database
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If for some reason you have a working database without South installed, add
south to the INSTALLED_APPS, then::

    manage.py syncdb # it will tell you to migrate the "tesi" app, DON'T DO ΙΤ!
    ./manage.py migrate tesi 0006 --fake
    vim tesi/models.py # add the Site.pleiades field
    ./manage.py schemamigration tesi --add-field=Site.pleiades # for some reason --auto doesn't work, who cares!
    ./manage.py migrate tesi
    vim tesi/models.py # add the Context.fastionline field
    ./manage.py schemamigration --auto tesi # now works!
    ./manage.py migrate tesi
