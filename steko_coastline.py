from django.conf import settings

from django.contrib.gis.geos import *
from django.contrib.gis.gdal import *

from tesi.models import *
from tesi.mpi import main

'''
ARSW in  Italy - Coastal sites
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This script can be executed straight from the terminal, without
running ``manage.py``. Just make sure the database server is up!

    DJANGO_SETTINGS_MODULE=settings python steko_coastline.py

'''

YEARS = xrange(399, 699, 10)

def get_quantitative():
    '''Extract quantitative data from each site.'''

    
    ds = DataSource('/home/steko/tesi/mappe/coastline/italy_coastline_buffer_10km.shp')
    lys = ds[0]
    bufgeom = lys[0].geom
    bgeo = fromstr(bufgeom.wkb)
    sites = Site.objects.filter(point__within=(bgeo))

    for y in YEARS:
        actual_sites = [ s for s in sites if s.final_import_date >= y and s.initial_import_date <= y ]
        print('%s done!' % y)

    types = Type.objects.filter(start_date__isnull=False
        ).filter(end_date__isnull=False)
    data = [
        [
            t.name,
            t.start_date,
            t.end_date,
            sum(sc.value for sc in t.sherdcount_set.filter(assemblage__context__site__in=actual_sites))]
        for t in types]
    table = main(data)
    print(table) # need to save this to a CSV file
    print('End')

def run():
    print('Anno,Siti')
    for y in YEARS:
        empire = Empire.objects.filter(periodo__gt=y).unionagg()
        total_sites = [ s for s in Site.objects.all() if s.final_import_date >= int(y) and s.initial_import_date <= int(y) ]
        sites = Site.objects.filter(point__within=empire)
        actual_sites = [ s for s in sites if s.final_import_date >= y and s.initial_import_date <= y ]
        print('%s,%s,%s' % (y, len(actual_sites),len(total_sites)))


if __name__ == "__main__":
    get_quantitative()
