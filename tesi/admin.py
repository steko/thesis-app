from tesi.models import *
from django.contrib.gis import admin

class TesiAdmin(admin.OSMGeoAdmin):
    default_lon =11
    default_lat =44
    default_zoom = 7
    save_on_top = True

class TypeAdmin(TesiAdmin):
    fieldsets = [
        (None,               {'fields': ['name',
                                         'slug',
                                         'form',
                                         'start_date',
                                         'end_date',
                                         'productions']
                             }),
        ('Extra', {'fields': ['subtype_of',
                              'start_hyp',
                              'end_hyp',
                              'description',
                              'biblio'],
                   'classes': ['collapse']}),
    ]
    prepopulated_fields = {'slug':('name',),}
    list_display = ('name', 'form', 'start_date', 'end_date')
    # list_filters = ('productions',)
    search_fields = ('name',)

class SiteAdmin(TesiAdmin):
    prepopulated_fields = {'slug':('name',),}

class ContextAdmin(TesiAdmin):
    list_display = ('name', 'site', 'settlement')
    list_filters = ('settlement',)

class SherdCountAdmin(TesiAdmin):
    fieldsets = [
        (None,               {'fields': ['mni',
                                         'count',
                                         'presence',
                                         'typename',
                                         'assemblage',
                                         'production']
                             }),
        ('Extra', {'fields': ['description',],
                   'classes': ['collapse']}),
    ]

    list_display = ('value', 'typename', 'assemblage', 'production')
    search_filters = ('typename', 'assemblage')
    list_filters = ('production',)

admin.site.register(Site, SiteAdmin)
admin.site.register(Context, ContextAdmin)
admin.site.register(Type, TypeAdmin)
admin.site.register(SherdCount, SherdCountAdmin)
admin.site.register(Assemblage, TesiAdmin)
admin.site.register(Production, TesiAdmin)
admin.site.register(Region, TesiAdmin)
admin.site.register(Empire, TesiAdmin)

