#! /usr/bin/env python
# -*- coding: utf-8 -*-
# plot.py

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.font_manager as fmg

small_font = fmg.FontProperties(size='xx-small')
#fentress = open('/home/steko/tesi/software/geodjango_media/fentress04.png.dat').readlines()
#fentress_values = [ l.split() for l in fentress ]
#fentress_x = [ i[0] for i in fentress_values ]
#fentress_y = [ i[1] for i in fentress_values ]

def graph(values, output, global_values=None):
    ind, val = values
    plt.figure(figsize=(6,4))
    ax1 = plt.gca()
    try:
        glob_ind, glob_val = global_values
    except TypeError:
        pass
    else:
        ax2 = ax1.twinx()
        f2 = ax2.fill(glob_ind, glob_val, 'k', alpha=0.2, lw=0.1, label='Media ponderata globale')
        ax2.set_ylabel('Media ponderata globale')
    f1 = ax1.fill(ind, val, 'g', alpha=0.5, lw=0.1, label='Media ponderata')
    ax1.set_xlim(390,710)
    ax1.set_ylim(ymax=max(val)*1.2)
    plt.grid(alpha=0.2)
    plt.xlabel('Anni')
    ax1.set_ylabel('Media ponderata')
    try:
        plt.legend( (f1, f2),
                    ('Media ponderata', 'Media ponderata globale'),
                    loc=0,
                    prop=small_font)
    except (ValueError, UnboundLocalError):
        plt.legend( ('Media ponderata globale',), loc=0, prop=small_font)
    plt.title('Distribuzione cronologica ARS, 400 - 700 AD')
    plt.savefig(output)
    fig = plt.gcf()
    fig.clear()

