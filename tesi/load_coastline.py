import os
from django.contrib.gis.utils import LayerMapping
from tesi.models import Coastline

coastline_shp = '/home/steko/tesi/mappe/coastline/italy_simple.shp'

# Auto-generated `LayerMapping` dictionary for CoastLine model
coastline_mapping = {
    'natural' : 'NATURAL',
    'name' : 'NAME',
    'geom' : 'LINESTRING',
}



def run(verbose=True):
    lm = LayerMapping(Coastline, coastline_shp, coastline_mapping,
                      transform=False, encoding='iso-8859-1')

    lm.save(strict=True, verbose=verbose)
