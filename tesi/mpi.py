#! /usr/bin/env python
# -*- coding: utf-8 -*-
# mpi.py

class mpiData:
    '''Initializes data structures for subsequent analyses.'''

    def __init__(self,dataframe,start_field,end_field,mult_field):
        for row in dataframe:
            try:
                int(row[end_field])
                int(row[start_field])
            except ValueError:
                pass
            else:
                self.ends = [ int(row[end_field]) for row in dataframe ]
                self.starts = [ int(row[start_field]) for row in dataframe ]
                self.mults = [ int(row[mult_field]) for row in dataframe ]

class mpiAnalysis:
    '''Here the actual counts and analysis is done. Output is ...
    
    Translated from the original R script. Very unpythonic.'''

    def __init__(self,ends,starts,mults,step=5):

        total_objects = len(ends)
        endt = 702 #int(max(starts)) - step/2
        startt = 2 #int(min(ends)) + step/2
        sequence = range(startt,endt,step)
        total_intervals = len(sequence)
        weighted_counts = []

        for k, value in enumerate(sequence):
            year = value
            weighted_counts.insert(k,0)
            for i in range(total_objects):
                if year < ends[i] and year > starts[i]:
                    prop = mults[i] * step / (ends[i] - starts[i] + 0.0)
                    weighted_counts[k] = weighted_counts[k] + prop

        self.yearslist = sequence
        self.wcounts = weighted_counts

        self.table = (self.yearslist,self.wcounts)

def mpi_main(data):
    '''Main loop.'''

    lista = data
    #lista.append(['inizio',0,1,1])
    lista.append(['fine',699,700,1])
    data = mpiData(lista,1,2,3)
    analysis = mpiAnalysis(data.ends,data.starts,data.mults)
    return analysis.table

def main_sites(data):
    pass
