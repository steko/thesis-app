import os
from django.contrib.gis.utils import LayerMapping
from models import Empire

empire_shp = '/home/steko/tesi/mappe/bizantini/inizio_7/inizio_7.shp'

# Auto-generated `LayerMapping` dictionary for Regioni model
empire_mapping = {
    'periodo' : 'periodo',
    'geom' : 'POLYGON',
}


def run(verbose=True):
    lm = LayerMapping(Empire, empire_shp, empire_mapping,
                      transform=False, encoding='iso-8859-1')

    lm.save(strict=True, verbose=verbose)
