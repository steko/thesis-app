# Create your views here.

import json

from urllib2 import urlopen

from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.gis.shortcuts import render_to_kml
from django.http import HttpResponse, Http404
from django.template import RequestContext
from django.core import serializers

from django.db.models import Count

from collections import defaultdict

from tesi.models import *
from tesi.mpi import *

def index(request):
    sites = Site.objects.all().order_by('name')
    return render_to_response('index.html', {'sites': sites},
        context_instance=RequestContext(request))

def all_kml(request):
    locations  = Site.objects.kml()
    return render_to_kml("placemarks.kml", {'places' : locations},
        context_instance=RequestContext(request)) 

def type_kml(request, type_id):
    '''Returns a KML with all the sites that contain an object of the
    given type.'''

    t = get_object_or_404(Type, pk=type_id)
    sites = []
    for sc in t.sherdcount_set.all():
        sites.append(sc.assemblage.context.site)
    # include also sub-types
    if t.type_set.all():
        for st in t.type_set.all():
            for sc in st.sherdcount_set.all():
                    sites.append(sc.assemblage.context.site)
            if st.type_set.all():
                for sst in st.type_set.all():
                    for sc in sst.sherdcount_set.all():
                            sites.append(sc.assemblage.context.site)
    siteset = set(sites)
    return render_to_kml("placemarks.kml", {'places': siteset},
        context_instance=RequestContext(request))

def type_kml_slug(request, type_slug):
    t = get_object_or_404(Type, slug=type_slug)
    return type_kml(request, t.id)

def type_map_page(request, type_id):
    lcount = Site.objects.all().count()
    t = get_object_or_404(Type, pk=type_id)
    return render_to_response('type_map.html', {'type': t},
            context_instance=RequestContext(request))

def type_map_page_slug(request, type_slug):
    t = get_object_or_404(Type, slug=type_slug)
    return type_map_page(request, t.id)

def map_page(request):
    """Displays a global map."""

    lcount = Site.objects.all().count()
    return render_to_response('map.html', {'location_count' : lcount},
        context_instance=RequestContext(request)) 

def sherdcount_index(request):
    scs = SherdCount.objects.all()
    total_scs = len(scs)
    total = sum(sc.value for sc in scs)
    types = Type.objects.all()
    return render_to_response('sherdcount_index.html',
        {'total': total, 'types': types, 'sherdcounts': total_scs},
        context_instance=RequestContext(request))

def sherdcount_list(request):
    """A simple table with all sherdcount objects, *not* paginated."""

    scs = SherdCount.objects.all()
    return render_to_response('sherdcount_list.html',
                              {'sherdcounts': scs },
        context_instance=RequestContext(request))

def site_index(request):
    """List sites in alphabetical order."""
    sites = Site.objects.all().order_by('name')
    number = Site.objects.all().count()
    missing = 0
    for s in sites:
        if s.total == 'm':
            missing += 1
    return render_to_response('site_index.html', {'sites': sites,
                                                        'number': number,
                                                        'missing': missing},
        context_instance=RequestContext(request))

def site_score(request):
    """List sites ordered by number of sherds."""
    
    sites = Site.objects.all().order_by('name')
    number = Site.objects.all().count()
    missing = 0
    for s in sites:
        if s.total == 'm':
            missing += 1
    return render_to_response('site_score.html', {'sites': sites,
                                                        'number': number,
                                                        'missing': missing},
        context_instance=RequestContext(request))

def context_settlements(request):
    """Rough summary about settlement classes."""

    ss = Site.objects.all()
    settlement_classes = {}
    for s in ss:
        site_classes = {}
        for c in s.context_set.all():
                 if c.settlement:
                     site_classes[ c.get_settlement_display() ] = True
        for c in site_classes:
             try:
                 settlement_classes[c] += 1
             except KeyError:
                 settlement_classes[c] = 1
    total = sum(v for k, v in settlement_classes.items())
    classes = [ {'class':c, 'number':n} for c, n in settlement_classes.items() ]
    return render_to_response('context_settlements.html',
                              {'settlement_classes': classes,
                               'total': total},
                              context_instance=RequestContext(request))

def site_detail(request, site_id):
    """Detailed view of single site, with global graph and counts."""
    
    s = get_object_or_404(Site, pk=site_id)
    forms = {}
    scs = SherdCount.objects.filter(assemblage__context__site=s
                                    )
    for sc in scs:
        if not sc.presence:
            if sc.typename.id in forms:
                forms[sc.typename.id] += sc.value
            else:
                forms[sc.typename.id] = sc.value
        else:
            if sc.typename.id in forms:
                pass
            else:
                forms[sc.typename.id] = True
    formsl = [ {'type':get_object_or_404(Type, pk=i),
                'count':c} for i, c in forms.items() ]
    graph_json = site_graph_data_json(site_id)
    return render_to_response('site_detail.html',
        {'site': s,
         'forms': formsl,
         'sherdcounts': scs,
         'graph_json': graph_json},
        context_instance=RequestContext(request))

def site_detail_slug(request, site_slug):
    s = get_object_or_404(Site, slug=site_slug)
    return site_detail(request, s.id)

def site_index_at_date(request, date):
    """Gives a list of all sites where ARS was still present at date."""

    sites_0 = Site.objects.all()
    sites = [ s for s in sites_0 if s.final_import_date >= int(date) and s.initial_import_date <= int(date) ]
    number = len(sites)
    settlement_classes = {}
    for s in sites:
        site_classes = {}
        for c in s.context_set.all():
                 if c.settlement:
                     site_classes[ c.get_settlement_display() ] = True
        for c in site_classes:
             try:
                 settlement_classes[c] += 1
             except KeyError:
                 settlement_classes[c] = 1
    total = sum(v for k, v in settlement_classes.items())
    classes = [ {'class':c, 'number':n} for c, n in settlement_classes.items() ]

    return render_to_response('site_index_at_date.html', 
                              {'date': date,
                               'sites': sites,
                               'total': total,
                               'number': number,
                               'settlement_classes': classes,
                               },
                              context_instance=RequestContext(request))

def site_kml_at_date(request, date):
    """Gives a map of all sites where ARS was still present at date."""

    sites_0 = Site.objects.all()
    sites = [ s for s in sites_0 if s.final_import_date >= int(date) and s.initial_import_date <= int(date)]

    return render_to_kml("placemarks.kml", {'places': sites})

def type_index(request):
    types = Type.objects.all().order_by('name')
    return render_to_response('type_index.html', {'types': types},
        context_instance=RequestContext(request))

def type_detail(request, type_id):
    t = get_object_or_404(Type, pk=type_id)
    productions = [ p for p in t.productions.all() ]
    for sc in t.sherdcount_set.all():
        if sc.production:
            productions.append(sc.production)
    productions = set(productions)
    sites = [ sc.assemblage.context.site for sc in t.sherdcount_set.all() ]
    sites = set(sites)
    return render_to_response(
        'type_detail.html',
        {'type': t,
         'productions': productions,
         'sites': sites,},
        context_instance=RequestContext(request))

def type_detail_slug(request, type_slug):
    t = get_object_or_404(Type, slug=type_slug)
    return type_detail(request, t.id)

def fentress_graph_image(request):
    response = HttpResponse(mimetype="image/png")
    types = Type.objects.filter(start_date__isnull=False
        ).filter(end_date__isnull=False)
    data = [ [t.name,
              t.start_date,
              t.end_date,
              sum( sc.value for sc in t.sherdcount_set.all() )] for t in types ]
    table = mpi_main(data)
    # FIXME image is broken
    return response

def fentress_graph_image_sites(request):
    response = HttpResponse(mimetype="image/png")
    types = Type.objects.filter(start_date__isnull=False
        ).filter(end_date__isnull=False)
    data = []
    for t in types:
        sites = []
        for sc in t.sherdcount_set.all():
            sites.append(sc.assemblage.context.site)
        sites = len(set(sites))
        data.append([t.name, t.start_date, t.end_date, sites])
    table = mpi_main(data)
    # FIXME image is broken
    return response

def fentress_graph_page(request):
    scs = SherdCount.objects.all()
    sites = Site.objects.all()
    contexts = Context.objects.all()
    total_sherds = sum( sc.value for sc in scs )
    total_sites = len([s for s in sites if s.total > 0])
    total_contexts = len([c for c in contexts if c.total > 0])
    return render_to_response('fentress_graph.html',
        {'total_sherds': total_sherds,
        'total_sites': total_sites,
        'total_contexts': total_contexts,
        },
        context_instance=RequestContext(request))

def context_detail(request, context_id):
    c = get_object_or_404(Context, pk=context_id)
    scs = SherdCount.objects.filter(assemblage__context=c
                                    )
    table = []
    forms = []
    for a in c.assemblage_set.all():
        for sc in a.sherdcount_set.all():
            forms.append(sc.typename)
    forms = set(forms)

    for a in c.assemblage_set.all():
        diko = {}
        diko['assemblage'] = a
        for sc in a.sherdcount_set.all():
            for f in forms:
                if f in diko:
                    if f == sc.typename:
                        try:
                            diko[f] += sc.value
                        except TypeError:
                            pass
                else:
                    if f != sc.typename:
                        diko[f] = 0
                    else:
                        if sc.presence:
                            diko[f] = 'presente'
                        else:
                            diko[f] = sc.value
        table.append(diko)

    titles = []
    cells = defaultdict(list)
    for x, col in enumerate(table):
        titles.append( col['assemblage'] )
        for rk in col:
            if rk == 'assemblage': continue # skip the title
            cells[rk].append ( col[rk] )
            if cells[rk][0] == rk: continue
            cells[rk].insert(0, rk)

    final = []
    for name in sorted( cells.keys(), key=lambda x:x.end_date ):
        final.append( cells[name] )

    return render_to_response('context_detail.html',
        {'context': c,
         'sherdcounts': scs,
         'final': final,
         'titles': titles,
         },
        context_instance=RequestContext(request))

def site_graph_image(request, site_slug):
    response = HttpResponse(mimetype="image/png")
    s = get_object_or_404(Site, slug=site_slug)
    types = Type.objects.filter(start_date__isnull=False
        ).filter(end_date__isnull=False)
    data = []
    for t in types:
        l = [t.name,
             t.start_date,
             t.end_date,
            sum( sc.value for sc in t.sherdcount_set.filter(
                    assemblage__context__site=s
                    )#.exclude(count__isnull=True)
                 )]
        data.append(l)
    data_global = [ [t.name,
                     t.start_date,
                     t.end_date,
                     sum( sc.value for sc in t.sherdcount_set.all() )] for t in types ]
    table = mpi_main(data)
    # FIXME image is broken
    return response

def site_graph_data(request, site_id):
    if request.method == 'GET':
        s = get_object_or_404(Site, id=site_id)
        types = Type.objects.filter(start_date__isnull=False
                                    ).filter(end_date__isnull=False)
        data = []
        for t in types:
            l = [t.name,
                 t.start_date,
                 t.end_date,
                 sum( sc.value for sc in t.sherdcount_set.filter(
                        assemblage__context__site=s
                        )
                      )]
            data.append(l)
        table = mpi_main(data)
        json = 'site_data = ' + json.dumps(table)
        response = HttpResponse(json, mimetype="application/json")
        return response

def site_graph_data_json(site_id):

    s = get_object_or_404(Site, id=site_id)
    types = Type.objects.filter(start_date__isnull=False
                                ).filter(end_date__isnull=False)
    data = []
    for t in types:
        l = [t.name,
             t.start_date,
             t.end_date,
             sum( sc.value for sc in t.sherdcount_set.filter(
                    assemblage__context__site=s
                    )
                  )]
        data.append(l)
    table = mpi_main(data)
    table = zip(*table)
    jsondata =json.dumps(table)
    return jsondata

def forms_graph_image(request):
    response = HttpResponse(mimetype="image/png")
    types = Type.objects.filter(start_date__isnull=False
        ).filter(end_date__isnull=False
        )#.filter(subtype_of__isnull=True)
    data = [ [t.name,
              t.start_hyp,
              t.start_date,
              t.end_date,
              t.end_hyp,
              None,
              t.total ] for t in types ]
    # FIXME image is broken
    return response

def forms_scatter_image(request, scatter_type):
    response = HttpResponse(mimetype="image/png")
    types0 = Type.objects.filter(start_date__isnull=False).filter(end_date__isnull=False).filter(form__isnull=False)
    types = [ t for t in types0  ]
    data = [ [t.name,
              t.start_hyp,
              t.start_date,
              t.end_date,
              t.end_hyp,
              t.form,
              t.total ] for t in types ]
    if scatter_type == '1':
        # FIXME image is broken
        pass
    elif scatter_type == '0':
        # FIXME image is broken
        pass
    return response

def notes_index(request):
    return render_to_response('notes_index.html',
        context_instance=RequestContext(request))

def type_graphs(request):
    return render_to_response('type_graphs.html',
        context_instance=RequestContext(request))


def prop_geojson(request):
    response = HttpResponse()
    qs = Site.objects.all()
    qs1 = Site.objects.extra(select={'geojson' : 'ST_AsGeoJSON(point)'})
    response.write("""{"type":"FeatureCollection","features":[""")
    for s in qs:
        try:
            s.point.json
        except AttributeError:
            pass
        else:
            response.write("""{"type":"Feature","id":%s,"geometry":""" % s.id )
            response.write(s.point.json)
            response.write(""","properties":{"name":"%s","value": %s}""" % (s.name, s.total))
            response.write("""},""")
    response.write("""]}""")
    return response

def prop_geojson2(request):
    sites  = Site.objects.all()
    return render_to_response("placemarks.json", {'sites' : sites},
        context_instance=RequestContext(request))

def map_prop(request):
     lcount = Site.objects.all().count()
     return render_to_response('map_prop.html', {'location_count' : lcount},
        context_instance=RequestContext(request)) 

def ware_index(request):
    wares = Production.objects.all()
    return render_to_response(
        "ware_index.html",
        {'wares': wares},
        context_instance=RequestContext(request))

def ware_detail(request, ware_id):
    w = get_object_or_404(Production, pk=ware_id)
    sites = []
    sherdcounts = [ sc for sc in w.sherdcount_set.all() ]
    for t in w.type_set.all():
        for sc in t.sherdcount_set.all():
            sites.append(sc.assemblage.context.site)
            sherdcounts.append(sc)
    for sc in w.sherdcount_set.all():
        sites.append(sc.assemblage.context.site)
    sites = set(sites)
    sherdcounts = set(sherdcounts)
    return render_to_response(
        "ware_detail.html",
        {'ware': w, 'sites': sites, 'sherdcounts': sherdcounts,},
        context_instance=RequestContext(request))

def context_bib(request, context_id, format="bib"):
    c = get_object_or_404(Context, pk=context_id)
    cites = c.biblio

    bib = '<!-- fetched from Zotero\n'
    bib += 'see http://www.zotero.org/support/dev/server_api/read_api -->'

    def zotero_read_item(url):
        '''Reads an item from the Zotero Read API.

        url is like https://www.zotero.org/steko/items/itemKey/UCI9Z59B'''

        urltemplate = "https://api.zotero.org/users/%(user)s/items/%(item)s?format=%(format)s&style=harvard-european-archaeology"
        params = {'user': 12096, 'item': '', 'format': format }
        parts = url.split('/')
        if 'itemKey' in parts:
            params['item'] = parts[parts.index('itemKey')+1]
        else:
            params['item'] = parts[parts.index('items')+1]
        uz = urlopen(urltemplate % params)
        return uz.read()

    biblio = [ {'biblio': zotero_read_item(c),
                'url': c} for c in cites.splitlines() ]
    return render_to_response(
        "biblio_snippet.html",
        {'biblio': biblio},
        context_instance=RequestContext(request))
