
from south.db import db
from django.db import models
from tesi.models import *

class Migration:
    
    def forwards(self):
        SETTLEMENT_CHOICES = (
            ('Ur', 'Urbano'),
            ('Va', 'Villa'),
            ('Ve', 'Villaggio'),
            ('Mi', 'Militare'),
            ('Co', 'Commerciale'),
            ('Ru', 'Rurale'),
            ('Se', 'Sepolcrale'),
            ('Ec', 'Ecclesiastico'),
            )
        
        # Adding field 'Context.settlement'
        db.add_column('tesi_context', 'settlement', models.CharField(max_length=2, choices=SETTLEMENT_CHOICES, null=True))
        
    
    def backwards(self):
        
        # Deleting field 'Context.settlement'
        db.delete_column('tesi_context', 'settlement')
        
