
from south.db import db
from django.db import models
from tesi.models import *

class Migration:
    
    def forwards(self):
        
        # Model 'Regions'
        db.create_table('tesi_regions', (
            ('id', models.FloatField(primary_key=True)),
            ('id_vmap0', models.FloatField()),
            ('f_code', models.CharField(max_length=4)),
            ('nam', models.CharField(max_length=43)),
            ('na2', models.CharField(max_length=1)),
            ('na3', models.CharField(max_length=32)),
            ('tile_id', models.FloatField()),
            ('fac_id', models.FloatField()),
            ('geom', models.PolygonField(srid=4326)),
        ))
        
        db.send_create_signal('tesi', ['Regions'])
    
    def backwards(self):
        db.delete_table('tesi_regions')
        
