
from south.db import db
from django.db import models
from tesi.models import *

class Migration:
    
    def forwards(self):
        
        # Adding field 'SherdCount.presence'
        db.add_column('tesi_sherdcount', 'presence', models.BooleanField('Presence', default=False, help_text="True if sherd count is not provided."))
        
    
    def backwards(self):
        
        # Deleting field 'SherdCount.presence'
        db.delete_column('tesi_sherdcount', 'presence')
        
