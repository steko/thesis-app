# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'Site.pleiades'
        db.add_column('tesi_site', 'pleiades', self.gf('django.db.models.fields.CharField')(default='', max_length=20, blank=True), keep_default=False)


    def backwards(self, orm):
        
        # Deleting field 'Site.pleiades'
        db.delete_column('tesi_site', 'pleiades')


    models = {
        'tesi.assemblage': {
            'Meta': {'ordering': "['context', 'name']", 'unique_together': "(('name', 'context'),)", 'object_name': 'Assemblage'},
            'biblio': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'context': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tesi.Context']"}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'end_date': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mni': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'start_date': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'total': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'tesi.coastline': {
            'Meta': {'object_name': 'Coastline'},
            'geom': ('django.contrib.gis.db.models.fields.LineStringField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'natural': ('django.db.models.fields.CharField', [], {'max_length': '80'})
        },
        'tesi.context': {
            'Meta': {'ordering': "['site', 'name']", 'object_name': 'Context'},
            'biblio': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'end_date': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'excavation': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'settlement': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tesi.Site']"}),
            'start_date': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'tesi.empire': {
            'Meta': {'object_name': 'Empire'},
            'geom': ('django.contrib.gis.db.models.fields.MultiPolygonField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'periodo': ('django.db.models.fields.IntegerField', [], {'max_length': '80'})
        },
        'tesi.production': {
            'Meta': {'ordering': "['name']", 'object_name': 'Production'},
            'area': ('django.contrib.gis.db.models.fields.PolygonField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'subproduction_of': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tesi.Production']", 'null': 'True', 'blank': 'True'})
        },
        'tesi.region': {
            'Meta': {'object_name': 'Region'},
            'f_code': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'fac_id': ('django.db.models.fields.FloatField', [], {}),
            'geom': ('django.contrib.gis.db.models.fields.MultiPolygonField', [], {}),
            'id': ('django.db.models.fields.FloatField', [], {'primary_key': 'True'}),
            'id_vmap0': ('django.db.models.fields.FloatField', [], {}),
            'na2': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'na3': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'nam': ('django.db.models.fields.CharField', [], {'max_length': '43'}),
            'tile_id': ('django.db.models.fields.FloatField', [], {})
        },
        'tesi.sherdcount': {
            'Meta': {'ordering': "['assemblage', 'typename']", 'unique_together': "(('typename', 'assemblage', 'production'),)", 'object_name': 'SherdCount'},
            'assemblage': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tesi.Assemblage']"}),
            'count': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mni': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'presence': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'production': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tesi.Production']", 'null': 'True', 'blank': 'True'}),
            'typename': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tesi.Type']"})
        },
        'tesi.site': {
            'Meta': {'ordering': "['name']", 'object_name': 'Site'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'pleiades': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'point': ('django.contrib.gis.db.models.fields.PointField', [], {'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'db_index': 'True', 'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        'tesi.type': {
            'Meta': {'ordering': "['name']", 'object_name': 'Type'},
            'biblio': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'end_date': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'end_hyp': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'form': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'productions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['tesi.Production']", 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'db_index': 'True', 'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'start_hyp': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'subtype_of': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tesi.Type']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['tesi']
