
from south.db import db
from django.db import models
from tesi.models import *

class Migration:
    
    def forwards(self):
        
        # Model 'Empire'
        db.create_table('tesi_empire', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('periodo', models.IntegerField(max_length=80)),
            ('geom', models.MultiPolygonField(srid=4326)),
        ))
        
        db.send_create_signal('tesi', ['Empire'])
    
    def backwards(self):
        db.delete_table('tesi_empire')
        
