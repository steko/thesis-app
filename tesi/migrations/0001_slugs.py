
from south.db import db
from django.db import models
from tesi.models import *

class Migration:
    
    def forwards(self):
        
        # Adding field 'Type.slug'
        db.add_column('tesi_type', 'slug', models.SlugField(null=True, blank=True))
        
        # Adding field 'Site.slug'
        db.add_column('tesi_site', 'slug', models.SlugField(null=True, blank=True))
        
    
    def backwards(self):
        
        # Deleting field 'Type.slug'
        db.delete_column('tesi_type', 'slug')
        
        # Deleting field 'Site.slug'
        db.delete_column('tesi_site', 'slug')
        
