def south_formula(site):
    """Compute the South formula for a site."""

    s = get_object_or_404(Site, slug=site_slug)
    types = Type.objects.filter(start_date__isnull=False
        ).filter(end_date__isnull=False)
    data = []
    for t in types:
        l = [t.name,
             t.start_date,
             t.end_date,
            sum( sc.count for sc in t.sherdcount_set.filter(
                    assemblage__context__site=s
                    ).exclude(count__isnull=True) )]
        data.append(l)
