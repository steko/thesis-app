var map, wms, vectors;

function init(){
    var lon = 5;
    var lat = 15;
    var zoom = 2;
    var symbol = new Geometry('circle', 10, 1312978855);

    var context = {
        getSize: function(feature) {
            return symbol.getSize(feature.attributes["value"]) * Math.pow(2,map.getZoom()-1);
        }
    };

    var template = {
		fillOpacity: 0.9,
        strokeColor: "#555555",
        strokeWidth: 1,
        pointRadius: "${getSize}", // using context.getSize(feature)
        fillColor: "#fae318"
    };

    var style = new OpenLayers.Style(template, {context: context});
    var styleMap = new OpenLayers.StyleMap({'default': style, 'select': {fillColor: '#ff6300'}});

    var options = {
        numZoomLevels: 6,
        controls: []  // Remove all controls
    };

    map = new OpenLayers.Map( 'map', options );

    wms = new OpenLayers.Layer.WMS( "OpenLayers WMS",
                                    "http://labs.metacarta.com/wms/vmap0?",
                                    {layers: 'basic'},
                                    {isBaseLayer: true} );

    vectors = new OpenLayers.Layer.GML( "Population", "../data/json/population_2005_symbols.json",
                                        { format: OpenLayers.Format.GeoJSON,
                                          styleMap: styleMap,
                                          isBaseLayer: false,
                                          projection: new OpenLayers.Projection("EPSG:4326"),
                                          attribution: "<a href='http://data.un.org'>UN Data</a>" } );

    map.addLayers([wms, vectors]);
    map.setCenter(new OpenLayers.LonLat(lon, lat), zoom);

    map.addControl(new OpenLayers.Control.LayerSwitcher());
    map.addControl(new OpenLayers.Control.MouseDefaults());
    map.addControl(new OpenLayers.Control.PanZoomBar());

    var options = {
        hover: true,
        onSelect: serialize };

    var select = new OpenLayers.Control.SelectFeature(vectors, options);
    map.addControl(select);
    select.activate();
}

function serialize() {
    var Msg = vectors.selectedFeatures[0].attributes["name"] + ": ";
    Msg    += vectors.selectedFeatures[0].attributes["value"];
    document.getElementById("info").innerHTML = Msg;
}