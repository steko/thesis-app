function init(){
    var lon = 5;
    var lat = 15;
    var zoom = 2;

	var options = {
        numZoomLevels: 6,
        controls: []  // Remove all controls
    };

    map = new OpenLayers.Map( 'map', options );

    wms = new OpenLayers.Layer.WMS( "OpenLayers WMS",
                                    "http://labs.metacarta.com/wms/vmap0?",
                                    {layers: 'basic'},
                                    {isBaseLayer: true} );

    var symbols = new OpenLayers.Layer.GML( "KML", "../data/kml/population_2005_symbols.kml",
                                        { format: OpenLayers.Format.KML,
                                          formatOptions: {
	                                          extractStyles: true,
	                                          extractAttributes: true }});
    map.addLayers([wms, symbols]);
    map.setCenter(new OpenLayers.LonLat(lon, lat), zoom);

    map.addControl(new OpenLayers.Control.LayerSwitcher());
    map.addControl(new OpenLayers.Control.MouseDefaults());
    map.addControl(new OpenLayers.Control.PanZoomBar());
}



