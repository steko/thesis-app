/* Copyright (c) 2008 Bjorn Sandvik, published under the LGPL license.
 */


function Geometry(symbol, maxSize, maxValue){
    this.symbol = symbol;
    this.maxSize = maxSize;
    this.maxValue = maxValue;

    this.getSize = function(value){
        switch(this.symbol) {
            case 'circle': // Returns radius of the circle
            case 'square': // Returns length of a side
                return Math.sqrt(value/this.maxValue)*this.maxSize;
            case 'bar': // Returns height of the bar
                return (value/this.maxValue)*this.maxSize;
            case 'sphere': // Returns radius of the sphere
            case 'cube': // Returns length of a side
                return Math.pow(value/this.maxValue, 1/3)*this.maxSize;
        }
    }
}



