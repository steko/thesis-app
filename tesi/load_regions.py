import os
from django.contrib.gis.utils import LayerMapping
from models import Region

regions_shp = '/home/steko/tesi/mappe/regione/studiate_dissolve.shp'

# Auto-generated `LayerMapping` dictionary for Regioni model
regions_mapping = {
   'id' : 'ID',
   'id_vmap0' : 'ID_VMAP0',
   'f_code' : 'F_CODE',
   'nam' : 'NAM',
   'na2' : 'NA2',
   'na3' : 'NA3',
   'tile_id' : 'TILE_ID',
   'fac_id' : 'FAC_ID',
   'geom' : 'MULTIPOLYGON',
}

def run(verbose=True):
    lm = LayerMapping(Region, regions_shp, regions_mapping,
                      transform=False, encoding='iso-8859-1')

    lm.save(strict=True, verbose=verbose)
