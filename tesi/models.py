from django.contrib.gis.db import models

# Create your models here.

class Site(models.Model):
    point = models.PointField(srid=4326,blank=True, null=True)
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True)
    pleiades = models.CharField(max_length=20, blank=True)
    objects = models.GeoManager()
    slug = models.SlugField(null=True, blank=True)

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return self.name

    def _count_total(self):
        """Return total number of sherds for the site."""
        cs = self.context_set.all()
        if cs:
            total = sum(c.total for c in cs)
        else:
            total = 'm'
        return total
    total = property(_count_total)

    def _final_import_date(self):
        """Final date of ARS import at site."""

        scs = SherdCount.objects.filter(assemblage__context__site=self)
        forms_lst = [ sc.typename for sc in scs ]
        forms = set(forms_lst)
        if forms:
            try:
                final_end = max(f.end_date for f in forms if f.end_date)
            except ValueError:
                final_end = None
            try:
                final_hyp = max(f.end_hyp for f in forms if f.end_hyp)
            except ValueError:
                final_date = final_end
            else:
                final_date = max(final_end, final_hyp)
        else:
            final_date = None
        return final_date
    final_import_date = property(_final_import_date)

    def _initial_import_date(self):
        """Initial date of ARS import at site."""

        scs = SherdCount.objects.filter(assemblage__context__site=self)
        forms_lst = [ sc.typename for sc in scs ]
        forms = set(forms_lst)
        if forms:
            try:
                initial_start = min(f.start_date for f in forms if f.start_date)
            except ValueError:
                initial_start = None
            try:
                initial_hyp = min(f.start_hyp for f in forms if f.start_hyp)
            except ValueError:
                initial_date = initial_start
            else:
                initial_date = min(initial_start, initial_hyp)
        else:
            initial_date = None
        return initial_date
    initial_import_date = property(_initial_import_date)

class Production(models.Model):
    name = models.CharField(max_length=50, unique=True)
    subproduction_of = models.ForeignKey('self', blank=True, null=True)
    area = models.PolygonField(srid=4326,blank=True, null=True)
    description = models.TextField(blank=True)
    objects = models.GeoManager()

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return self.name


class Context(models.Model):
    EXCAVATION_CHOICES = (
        ('Ex','Excavation'),
        ('Su', 'Survey'),
        ('Sh', 'Shovel test'),
        )
    SETTLEMENT_CHOICES = (
        ('Ur', 'Urbano'),
        ('Va', 'Villa'),
        ('Ve', 'Villaggio'),
        ('Mi', 'Militare'),
        ('Co', 'Commerciale'),
        ('Ru', 'Rurale'),
        ('Se', 'Sepolcrale'),
        ('Ec', 'Ecclesiastico'),
        ('Gr', 'Grotta'),
        ('ND', 'Non definito'),
        )

    site = models.ForeignKey(Site)
    name = models.CharField(max_length=100)
    excavation = models.CharField(max_length=2, choices=EXCAVATION_CHOICES)
    start_date = models.IntegerField(blank=True, null=True)
    end_date = models.IntegerField(blank=True, null=True)
    description = models.TextField(blank=True)
    biblio = models.TextField(blank=True)
    settlement = models.CharField(max_length=2, choices=SETTLEMENT_CHOICES, null=True)
    fastionline = models.CharField(max_length=100,
                                   blank=True,
                                   help_text='Do not put the entire URL here, only the site ID. If the complete URL is http://www.fastionline.org/micro_view.php?fst_cd=AIAC_2594 then the side ID is AIAC_2594.')
    objects = models.GeoManager()

    class Meta:
        ordering = ['site', 'name']

    def __unicode__(self):
        return u'%s, %s' % (self.site.name, self.name)

    def _count_total(self):
        """Return total number of sherds for the context."""
        total = sum(a.total_count for a in self.assemblage_set.all())
        return total
    total = property(_count_total)


class Assemblage(models.Model):
    name = models.CharField(max_length=50)
    context = models.ForeignKey(Context)
    start_date = models.IntegerField(blank=True, null=True)
    end_date = models.IntegerField(blank=True, null=True)
    total = models.IntegerField(blank=True, null=True)
    mni = models.IntegerField(blank=True, null=True)
    description = models.TextField(blank=True)
    biblio = models.TextField(blank=True)
    objects = models.GeoManager()

    class Meta:
        ordering = ['context', 'name']
        unique_together = (('name', 'context'),)

    def __unicode__(self):
        return u'%s, %s : %s' % (self.context.site.name,
                                  self.context.name,
                                  self.name)

    def _count_total(self):
        """Return total number of sherds for the assemblage."""
        total = sum(sc.value for sc in self.sherdcount_set.all())
        return total
    total_count = property(_count_total)

class Type(models.Model):
    name = models.CharField(max_length=100, unique=True)
    subtype_of = models.ForeignKey('self', blank=True, null=True)
    start_date = models.IntegerField(blank=True, null=True)
    end_date = models.IntegerField(blank=True, null=True)
    start_hyp = models.IntegerField(blank=True, null=True)
    end_hyp = models.IntegerField(blank=True, null=True)
    productions = models.ManyToManyField(Production, blank=True, null=True)
    description = models.TextField(blank=True)
    biblio = models.TextField(blank=True)
    form = models.CharField(max_length=50)
    slug = models.SlugField(null=True, blank=True)
    objects = models.GeoManager()

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return self.name

    def _count_total(self):
        """Return total number of sherds for the type."""
        total = sum(sc.value for sc in self.sherdcount_set.all())
        return total
    total = property(_count_total)

    def _count_sites(self):
        """Count the total number of sites where the type is present."""

        sites = [ sc.assemblage.context.site for sc in self.sherdcount_set.all() ]
        sites = set(sites)
        return len(sites)
    total_sites = property(_count_sites)

    def at_site(self, site):
        """Return the value for form f at site s."""

        scs = SherdCount.objects.filter(typename=self).filter(assemblage__context__site=site)
        value = sum(sc.value for sc in scs)
        return value

class SherdCount(models.Model):
    """SherdCount objects represent a single type in an assemblage.

    They provide the number of sherds, the MNV or a presence boolean
    in the worst case. The best available value is chosen among the
    three for analysis and display of data."""

    mni = models.PositiveIntegerField('MNI', blank=True, null=True)
    count = models.PositiveIntegerField('Sherd count', blank=True, null=True)
    presence = models.BooleanField('Presence', default=False, help_text="True if sherd count is not provided.")

    typename = models.ForeignKey(Type, verbose_name='Form')
    assemblage = models.ForeignKey(Assemblage)
    description = models.TextField(blank=True)
    production = models.ForeignKey(Production, blank=True, null=True, verbose_name='Ware')
    objects = models.GeoManager()

    class Meta:
        ordering = ['assemblage', 'typename']
        unique_together = (('typename', 'assemblage', 'production'),)

    def __unicode__(self):
        result = u'%s, %s : ' % (self.assemblage.context.name, self.assemblage.name,)
        if self.count:
            result += u'%d ' % self.count
        if self.presence:
            result += u'presente '
        result += u'%s ' % self.typename
        if self.production:
            result += u'(%s)' % self.production
        return result

    @property
    def value(self):
        """Chooses the best available number for this sherdcount."""

        if not self.mni:
            if not self.count:
                return int(self.presence)
            else:
                return self.count
        else:
            return self.mni
 
# This is an auto-generated Django model module created by ogrinspect.
class Region(models.Model):
   id = models.FloatField(primary_key=True)
   id_vmap0 = models.FloatField()
   f_code = models.CharField(max_length=4)
   nam = models.CharField(max_length=43)
   na2 = models.CharField(max_length=1)
   na3 = models.CharField(max_length=32)
   tile_id = models.FloatField()
   fac_id = models.FloatField()
   geom = models.MultiPolygonField(srid=4326)
   objects = models.GeoManager()

   def __unicode__(self):
       return self.nam

class Empire(models.Model):
    periodo = models.IntegerField(max_length=80)
    geom = models.MultiPolygonField(srid=4326)
    objects = models.GeoManager()

class Coastline(models.Model):
    natural = models.CharField(max_length=80)
    name = models.CharField(max_length=80)
    geom = models.LineStringField(srid=4326)
    objects = models.GeoManager()
