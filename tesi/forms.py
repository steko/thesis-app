#!/usr/bin/env python
# a stacked bar plot with errorbars

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager as fmg

import csv

small_font = fmg.FontProperties(size='xx-small')

def forms_graph(values, output):

    tortarray = np.array(values)

    """['Hayes 94,3' '425' '450' '500' '525' 'Bowl' 'Uncommon']"""

    N = len(tortarray)

    tortarray = tortarray[tortarray[:,2].argsort(),]

    starts = tortarray[:,2].astype('d')
    ends   = tortarray[:,3].astype('d')
    names  = tortarray[:,0]
    frequency = tortarray[:,6].astype('d')

#    forms = tortarray[:,5]

    freq_map = {
        'Rare' : 0.2,
        'Rather uncommon' : 0.4,
        'Uncommon' : 0.3,
        'Fairly common' : 0.6,
        'Common' : 0.8,
        'Rather common' : 0.5,
        'Very common' : 1.0,
        '' : 0.2
        }

    form_map = {
        'dish' : '#c90000',
        'plate' : '#00c900',
        'bowl' : '#0000c9',
        }

    for i in tortarray:
        if i[1] == '':
            i[1] = i[2]
        if i[4] == '':
            i[4] = i[3]

    min_date = tortarray[:,1].astype('d')
    max_date = tortarray[:,4].astype('d')

#    width = [ freq_map[i] for i in frequency ]
#    colors = [form_map[i.split()[-1].lower()] for i in forms ]
    ind = np.arange(N) + 1   # the x locations for the groups
    plt.figure(figsize=(12,9))
    p0 = plt.bar(ind, max_date - min_date, width=0.02, bottom=min_date, align='center', color='g', lw=0, alpha=0.3)
#    p1 = plt.bar(ind, ends - starts, width=0.6, bottom=starts, align='center', color='66b366', lw=0,)
    p1 = plt.bar(ind,
                 ends - starts,
                 width=np.log(frequency)/(ends - starts + 1)*30 + 0.1,
                 # freak viz
                 #width=frequency/(ends - starts + 1)*30 + 0.1,
                 bottom=starts,
                 align='center',
                 color='g',
                 lw=0,
                 alpha=0.5)

    plt.ylabel('Anni')
    plt.title('Cronologie ARS')
    plt.xticks(ind, names, rotation=90, fontsize=5 )
    for i in ind:
        plt.text(i, ends[i-1], names[i-1], rotation=45, fontsize=5 )
    plt.grid(ls='-', alpha=0.05)
    #plt.grid(ydata = np.array([0]), ls='-', alpha=0.2)
#    plt.legend(('dish', 'plate', 'bowl'), loc='upper left' )
    plt.savefig(output)
    fig = plt.gcf()
    fig.clear()

def forms_scatter_hard(values, output):
    tortarray = np.array(values)
    N = len(tortarray)
    tortarray = tortarray[tortarray[:,2].argsort(),]
    starts = tortarray[:,2].astype('d')
    ends   = tortarray[:,3].astype('d')
    names  = tortarray[:,0]
    forms = tortarray[:,5]
    frequency = tortarray[:,6].astype('d')

    form_map = ('#c90000', '#00c900', '#0000c9', '#c9c9c9')
    def fmap(form):
        try:
            text = form.split()[-1].lower()
            if 'dish' in text:
                return form_map[0]
            elif 'plate' in text:
                return form_map[1]
            elif 'bowl' in text:
                return form_map[2]
            else:
                return form_map[3]
        except IndexError:
            return form_map[3]
    colors = map(fmap,forms)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    # ugly hack for getting a decent legend
    # f_dish = ax.scatter( starts, ends, s=np.sqrt(frequency)*20, facecolor=form_map[0], lw=0, label='Dish')
    # f_plate = ax.scatter( starts, ends, s=np.sqrt(frequency)*20, facecolor=form_map[1], lw=0, label='Plate')
    # f_bowl = ax.scatter( starts, ends, s=np.sqrt(frequency)*20, facecolor=form_map[2], lw=0, label='Bowl')
    # f_unknown = ax.scatter( starts, ends, s=np.sqrt(frequency)*20, facecolor=form_map[3], lw=0, label='Undefined')
    # fig.clear()
    # ax = fig.add_subplot(111)
#    for i in form_map:
#        ax.plot(0,0, i)
#    plt.legend(('dish', 'plate', 'bowl','unknown'), loc=2 )
    ax.scatter( starts,
                ends,
                #marker=(xy1,0),
                s=np.sqrt(frequency)*20,
                alpha=0.3,
                facecolor=colors,
                lw=0,
                #label=('Dish', 'Plate', 'Bowl', 'Undefined',)
                )

    plt.grid(ls='-', alpha=0.05)
#    for i in tortarray:
#        plt.text(i[2], i[3], i[0], fontsize=5,
#        rotation=45,
#        alpha=0.3 )
    plt.xlabel('Comparsa della forma')
    plt.ylabel('Scomparsa della forma')
    plt.title('Comparsa e scomparsa delle forme')
    # plt.legend( (f_dish, f_plate, f_bowl, f_unknown,),
    #             ('Dish', 'Plate', 'Bowl', 'Undefined',),
    #             #prop=small_font,
    #             loc=2 )
    plt.savefig(output)
    fig.clear()

def forms_scatter_simple(values, output):
    tortarray = np.array(values)
    N = len(tortarray)
    tortarray = tortarray[tortarray[:,2].argsort(),]
    starts = tortarray[:,2].astype('d')
    ends   = tortarray[:,3].astype('d')
    names  = tortarray[:,0]
    forms = tortarray[:,5]
    frequency = tortarray[:,6].astype('d')

    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.scatter( starts,
                ends,
                #marker=(xy1,0),
                s=np.sqrt(frequency)*20,
                alpha=0.3,
                facecolor='g',
                lw=0)

    plt.xlabel('Comparsa della forma')
    plt.ylabel('Scomparsa della forma')
    plt.title('Comparsa e scomparsa delle forme')
    plt.savefig(output)
    fig.clear()

