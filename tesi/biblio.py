import _bibtex as bib
sdf = """@Proceedings{MPL06,
  Title          = {Old pottery in a new century. {I}nnovating
                   perspectives on {R}oman pottery studies. {A}tti del
                   convegno internazionale di studi, {C}atania 22-24
                   {A}prile 2004},
  Editor         = {Malfitana, Daniele and Poblome, Jeroen and Lund, John},
  year           = 2006
}"""


#db = bib.open_string(sdf, 'r', 0)
db = bib.open_file('/home/steko/tesi/latex/tesi2.bib', 0)

#dbl = [ b for b in bib.next(db) ]

def steko():
    while True:
        try:
            bib_entry = bib.next(db)
        except:
            print('fail')
            break
        else:
            try:
                l = len(bib_entry)
            except TypeError:
                break
            else:
                #print(bib_entry)
                if bib_entry[1] == 'inproceedings':
                    bf = bib_entry[4]
                    print(bib.get_native(bf['author']))

def checkfile (filename, strict = 1, typemap = {}):

    def expand (file, entry, type = -1):

        items = entry [4]
        for k in items.keys ():
            items [k] = _bibtex.expand (file, items [k], typemap.get (k, -1))

