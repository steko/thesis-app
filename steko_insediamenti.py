from tesi.models import *
import StringIO

from operator import itemgetter

def sites_at_date(date):
    sites_0 = Site.objects.all()
    sites = [ s for s in sites_0 if s.final_import_date >= int(date) and s.initial_import_date <= int(date) ]

    settlement_classes = {
        'Commerciale':0,
        'Ecclesiastico':0,
        'Grotta':0,
        'Militare':0,
        'Non definito':0,
        'Rurale':0,
        'Sepolcrale':0,
        'Urbano':0,
        'Villa':0,
        'Villaggio':0
        }
    for s in sites:
        site_classes = {}
        for c in s.context_set.all():
            if c.settlement:
                site_classes[ c.get_settlement_display() ] = True
        for c in site_classes:
            settlement_classes[c] += 1

    classes = [ {'class':c, 'number':n} for c, n in settlement_classes.items() ]

    return sorted(classes, key=itemgetter('class'))

dates = xrange(400, 700, 10)

def sad_table():
    result = StringIO.StringIO()
    result.write("Anno,Commerciale,Ecclesiastico,Grotta,Militare,Non definito,Rurale,Sepolcrale,Urbano,Villa,Villaggio,x\n")

    for d in dates:
        d_list = sites_at_date(d)
        result.write("%s," % d)
        for i in d_list:
                result.write("%(number)s," % i),
        result.write("\n")

    salva = open("insediamenti.csv", 'w')
    salva.write(result.getvalue())
    salva.close()
