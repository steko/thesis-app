from tesi.models import *
import csv

sites = Site.objects.order_by('name')
forms = Type.objects.order_by('name')

header = [str(f.name) for f in forms]
header.insert(0, 'siti')

matrice = []
matrice.append(header)

for s in sites:
    if not s.total == 'm' and s.total > 1:
        row = [s.name]
        for f in forms:
            row.append(f.at_site(s))
        matrice.append(row)

print matrice

salva = open("matrice.csv", 'w')
writer = csv.writer(salva)
for row in matrice:
#    salva.write(','.join([ str(i) for i in row ]))
#    salva.write('\n')
    writer.writerow(row)
salva.close()
